@extends('welcome')
@section('transaction')
    active
@endsection
@section('idelogi')
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-xs-12 mb-2">
                    <h3 class="content-header-title mb-0">Transaction</h3>
                </div>
            </div>

            <div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
                    <div class="row">
                        <div class="col-xs-12">
                            {{$respone->id}}
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->
            </div>
        </div>
    </div>
@endsection
@section('foot')
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{asset('app-assets/js/scripts/ui/breadcrumbs-with-stats.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
@stop
