<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',                     'homeController@home');

Route::get('/add-entry',            'AddEntryController@addEntry');

Route::get('/transaction',         'transactionController@transaction');
Route::get('/transaction/{id}',         'transactionController@show')->name('transaction.show');
