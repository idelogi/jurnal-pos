@extends('welcome')
@section('addentry')
    active
@endsection
@section('idelogi')
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-xs-12 mb-2">
                    <h3 class="content-header-title mb-0">Horizontal Forms</h3>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <form class="form form-horizontal">
                            <div class="form-body">
                                <h4 class="form-section"><i class="ft-user"></i> About User</h4>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Fist Name</label>
                                            <select class="select2 form-control">
                                                <optgroup label="Alaskan/Hawaiian Time Zone">
                                                    <option value="AK">Alaska</option>
                                                    <option value="HI">Hawaii</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Fist Name</label>
                                            <input type="text" id="userinput1" class="form-control"
                                                   placeholder="First Name" name="firstname">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <h2>Rp 30,000,000</h2>
                                        </div>
                                    </div>
                                </div>

                                <h4 class="form-section"><i class="ft-mail"></i> Contact Info & Bio</h4>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Billing Address</label>
                                            <textarea id="userinput8" rows="6"
                                                      class="form-control" name="bio"
                                                      placeholder="Bio"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Transaction Date</label>
                                            <input type='text' class="form-control" id="datetimepicker4"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Due Date</label>
                                            <input type='text' class="form-control" id="datetimepicker5"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Term</label>
                                            <select class="select2 form-control border-primary">
                                                <optgroup label="Alaskan/Hawaiian Time Zone">
                                                    <option value="AK">Alaska</option>
                                                    <option value="HI">Hawaii</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Transaction No</label>
                                            <input class="form-control" type="text" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Vendor Ref No</label>
                                            <input class="form-control" type="text">
                                        </div>
                                    </div>
                                </div>

                                <h4 class="form-section"><i class="ft-mail"></i> Contact Info & Bio</h4>

                                <section id="form-repeater">
                                    <div class="repeater-default">
                                        <div data-repeater-list="car">
                                            <div data-repeater-item>
                                                <div class="table-responsive">
                                                    <table class="table table-column table-xs">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="form-group">
                                                                    <label>Product</label>
                                                                    <input type="text"
                                                                           class="form-control">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <label>Description</label>
                                                                    <input type="text"
                                                                           class="form-control">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <label>Qty</label>
                                                                    <input class="form-control"
                                                                           type="text">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <label>Units</label>
                                                                    <input class="form-control"
                                                                           type="text">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <label>Unit Price</label>
                                                                    <input class="form-control"
                                                                           type="text">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <label>Tax</label>
                                                                    <input class="form-control"
                                                                           type="text">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <label>Amount</label>
                                                                    <input class="form-control"
                                                                           type="text">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group col-sm-12 col-md-2 text-xs-center mt-2">
                                                                    <button type="button"
                                                                            class="btn btn-danger"
                                                                            data-repeater-delete><i
                                                                                class="ft-x"></i>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group overflow-hidden">
                                            <div class="col-xs-12">
                                                <button data-repeater-create
                                                        class="btn btn-primary btn-lg">
                                                    <i class="icon-plus4"></i> Add
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <h4 class="form-section"><i class="ft-user"></i> About User</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Massage</label>
                                            <textarea id="userinput8" rows="3"
                                                      class="form-control" name=""
                                                      placeholder=""></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>Memo</label>
                                            <textarea id="userinput8" rows="3"
                                                      class="form-control" name=""
                                                      placeholder=""></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h5 class="mt-2">
                                                Subtotal <span class="pull-right">Rp 30,000,000</span>
                                            </h5>
                                            <h5 class="mt-1">
                                                Total <span class="pull-right">Rp 30,000,000</span>
                                            </h5>
                                            <h3 class="mt-1">
                                                Balance Due <span class="pull-right">Rp 30,000,000</span>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions right">
                                <button type="button" class="btn btn-warning mr-1">
                                    <i class="ft-x"></i> Cancel
                                </button>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-check-square-o"></i> Save
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            <div class="content-body"><!-- Basic form layout section start -->

            </div>
        </div>
    </div>
@endsection
@section('foot')
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset('app-assets/js/scripts/pickers/dateTime/picker-date-time.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <script src="{{asset('app-assets/js/scripts/forms/form-repeater.js')}}" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset('app-assets/js/scripts/forms/select/form-select2.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
@stop