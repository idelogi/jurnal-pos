<div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class=" navigation-header">
                <span>General</span>
            </li>
            <li class="@yield('home')">
                <a href="{{url('/')}}" class="menu-item">
                    <i class="ft-home"></i><span data-i18n="" class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class="@yield('addentry')">
                <a href="{{url('/add-entry')}}" class="menu-item">
                    <i class="ft-file-plus"></i><span data-i18n="" class="menu-title">Add Entry</span>
                </a>
            </li>
            <li class="@yield('transaction')">
                <a href="{{url('/transaction')}}" class="menu-item">
                    <i class="ft-list"></i><span data-i18n="" class="menu-title">Transaction</span>
                </a>
            </li>

            {{--<li class=" nav-item">--}}
                {{--<a href="#">--}}
                    {{--<i class="ft-monitor"></i>--}}
                    {{--<span data-i18n="" class="menu-title">Create Entry</span>--}}
                {{--</a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li><a href="#" class="menu-item">Vertical</a>--}}
                        {{--<ul class="menu-content">--}}
                            {{--<li>--}}
                                {{--<a href="../vertical-modern-menu-template" class="menu-item">Modern Menu</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a href="#" class="menu-item">Horizontal</a>--}}
                        {{--<ul class="menu-content">--}}
                            {{--<li>--}}
                                {{--<a href="../horizontal-menu-template" class="menu-item">Classic</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>