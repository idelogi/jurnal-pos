@extends('welcome')
@section('transaction')
    active
@endsection
@section('idelogi')
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-xs-12 mb-2">
                    <h3 class="content-header-title mb-0">Transaction</h3>
                </div>
            </div>

            <div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">All Transaction</h4>
                                    <a class="heading-elements-toggle"><i
                                                class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body collapse in">
                                    <div class="card-block card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Number</th>
                                                <th>Vendor</th>
                                                <th>Due Date</th>
                                                <th>Status</th>
                                                <th>Balance Due(in IDR)</th>
                                                <th>Total(in IDR)</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                              @foreach ($respone->purchase_invoices as $respone)
                                                <tr>
                                                  <td> {{$respone->transaction_date}}</td>
                                                  <td>
                                                    <a href="{{route('transaction.show',$respone->id)}}">{{$respone->transaction_no}}</a>
                                                  </td>
                                                  <td> {{$respone->person->display_name}}</td>
                                                  <td> {{$respone->due_date}}</td>
                                                  <td> {{$respone->transaction_status->name_bahasa}}</td>
                                                  <td> {{$respone->remaining_currency_format}}</td>
                                                  <td> {{$respone->original_amount_currency_format}}</td>
                                                </tr>
                                              @endforeach
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->
            </div>
        </div>
    </div>
@endsection
@section('foot')
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{asset('app-assets/js/scripts/ui/breadcrumbs-with-stats.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
@stop
