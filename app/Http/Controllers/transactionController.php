<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\JsonStatus;
use App\Http\Requests;


class transactionController extends Controller
{
    //
    public $client;
    public function __construct()
    {
        $this->client = new  \GuzzleHttp\Client(['base_uri' => 'http://sandbox-api.jurnal.id/partner/core/api/v1/']);
    }

    public function transaction(){
      try{
        $getResponse = $this->client->request('GET','purchase_invoices?locale=en&access_token=b49f9d0f482d4fde9c67294a40aea26f');
        $getResponse = json_decode($getResponse->getBody());
        // if ($getResponse->diagnostic->status == 200){
          // $airport = $getResponse->all_airport->airport;
          //  return $getResponse->purchase_invoices[0]->transaction_no;
        // }else{
        //   $airport = null;
        // }
      }catch (\Exception $e){
        return JsonStatus::messageException($e);
      }

      return view('vendor.pages.transaction',['respone'=>$getResponse]);
    }
    public function show($id)
    {
      try{
        $getResponse = $this->client->request('GET','purchase_invoices/'.$id.'?locale=en&access_token=b49f9d0f482d4fde9c67294a40aea26f');
        $getResponse = json_decode($getResponse->getBody());
      }catch (\Exception $e){
        return JsonStatus::messageException($e);
      }
      return view('vendor.pages.show',['respone'=>$getResponse->purchase_invoice]);
    }
}
